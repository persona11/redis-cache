package com.learning.redis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learning.redis.model.User;
import com.learning.redis.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/user")
	public User addUser(@RequestBody User user) {
		return userService.addUser(user);
	}

	@PutMapping("/user")
	public User updateUser(@RequestBody User user, @RequestParam int userId) throws Exception {
		return userService.updateUser(user, userId);
	}

	@DeleteMapping("/user")
	public String deleteUser(@RequestParam int userId) throws Exception {
		return userService.deleteUser(userId);
	}

	@GetMapping("/user")
	public User getUser(@RequestParam int userId) throws Exception {
		return userService.getUser(userId);
	}

	@GetMapping("/users")
	public List<User> getUsers() {
		return userService.getUsers();
	}
}
