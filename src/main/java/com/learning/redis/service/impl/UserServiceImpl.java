package com.learning.redis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learning.redis.config.CacheServer;
import com.learning.redis.model.User;
import com.learning.redis.repository.UserRepository;
import com.learning.redis.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private CacheServer<String, User> cacheServer;

	@Autowired
	private UserRepository userRepo;

	@Override
	public User addUser(User user) {
		return userRepo.save(user);
	}

	@Override
	public User updateUser(User user, int userId) throws Exception {
		User u = userRepo.findById(userId).orElseThrow(() -> new Exception("user not found"));
		u.setUserName(user.getUserName());
		u.setUserAge(user.getUserAge());
		return userRepo.save(u);
	}

	@Override
	public String deleteUser(int userId) throws Exception {
		User u = userRepo.findById(userId).orElseThrow(() -> new Exception("user not found"));
		userRepo.delete(u);
		return "deleted successfully";
	}

	@Override
	public User getUser(int userId) throws Exception {
		User u = cacheServer.getValue(String.valueOf(userId));
		if (u != null)
			return u;
		User user = userRepo.findById(userId).orElseThrow(() -> new Exception("user not found"));
		cacheServer.putValue(String.valueOf(userId), user);
		return user;
	}

	@Override
	public List<User> getUsers() {
		return userRepo.findAll();
	}

}
