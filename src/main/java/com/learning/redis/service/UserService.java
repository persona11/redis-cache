package com.learning.redis.service;

import java.util.List;

import com.learning.redis.model.User;

public interface UserService {

	public User addUser(User user);

	public User updateUser(User user, int userId) throws Exception;

	public String deleteUser(int userId) throws Exception;

	public User getUser(int userId) throws Exception;

	public List<User> getUsers();
}
