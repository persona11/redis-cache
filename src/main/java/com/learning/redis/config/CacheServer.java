package com.learning.redis.config;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class CacheServer<K, V> {

	private RedisTemplate<K, V> redisTemplate;

	public CacheServer(RedisTemplate<K, V> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public void putValue(K key, V value) {
		redisTemplate.opsForValue().set(key, value);
	}

	public V getValue(K key) {
		return redisTemplate.opsForValue().get(key);
	}

}
