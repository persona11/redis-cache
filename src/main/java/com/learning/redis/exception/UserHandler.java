package com.learning.redis.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UserHandler {

	@ExceptionHandler
	public Map<String, Object> exceptionHandler(Exception e) {
		Map<String, Object> map = new HashMap<>();
		map.put("error", e.getMessage());
		return map;
	}
}
